<?php
$arr = [];
for ($i = 0; $i < 100; $i++) {
    $arr[] = rand(1, 100);
}
function getMultiplyAll(array $arr): float
{
    $totalBig = 1;
    for ($i = 0; $i < 10; $i++) {
        if ($arr[$i] > 0 && $i % 2 == 0) {
            $totalBig *= $arr[$i];
        }
    }
    return $totalBig;
}

echo getMultiplyAll($arr);

function getMultiplyNotAll(array $arr): float
{
    $totalSmall = 1;
    for ($i = 0; $i < 10; $i++) {
        if ($arr[$i] > 0 && $i % 2 != 0) {
            $totalSmall *= $arr[$i];
        }
    }
    return $totalSmall;
}

echo getMultiplyNotAll($arr);

function getMultiNumber(float $a, float $b): float
{
    return $a * $b;
}

echo getMultiNumber(6, 5);

function getSumNumber(float $a, float $b): float
{
    return $a + $b;
}

echo getSumNumber(6, 5);

function getPow(float $a, float $b): float
{
    return $a ** $b;
}

echo getPow(6, 5);


function getAvg(float $a, float $b, float $c): float
{
    return ($a + $b + $c) / 3;
}

echo getAvg(8, 9, 5);

function getPercent(float $n, int $proc): float
{
    return ($n + ($n * ($proc / 100)));
}

echo  getPercent(50, 30);
echo getPercent(50, 120);

?>
    <form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
        <select name="country">
            <option value="Турция">Турция</option>
            <option value="Египет">Египет</option>
            <option value="Италия">Италия</option>
        </select><br>
        Введите количество дней:<br>
        <input type="number" name="day" id="">
        <br>
        Наличие скидки:
        <input type="checkbox" name="skidka">
        <br>
        <input type="submit" value="submit" name="submit1">
    </form>
<?php
if ($_REQUEST['submit1']) {
    $day = htmlspecialchars($_REQUEST['day']);
    $discount = $_REQUEST['skidka'];
    $country = $_REQUEST['country'];
    if ($discount == 'on') {
        $discount = 0.05;
    } else {
        $discount = 0;
    }
    if ($country == 'Египет') {
        $country = 0.1;
    } elseif ($country == 'Италия') {
        $country = 0.12;
    } else {
        $country = 0;
    }
    echo getLeaveDays($discount, $country, $day);
}

function getLeaveDays(float $discount, float $country, float $day)
{
    return $day * 400 + $day * 400 * $country - $day * 400 * $country * $discount;
}

if (@$_REQUEST['submit']) {
    $email = htmlspecialchars($_REQUEST['email']);
    $name = htmlspecialchars($_REQUEST['name']);
    $password = htmlspecialchars($_REQUEST['password']);
    getTrueForm($name, $email, $password);
}
function getTrueForm(string $a, string $b, string $c)
{
    $mas = ['Не введено имя пользователя' => $a, 'Не введен email' => $b, 'Не введен пароль' => $c];
    $coun = 0;
    foreach ($mas as $index => $item) {
        if (!$item) {
            echo $index . '<br>';
            $coun++;
        }
    }
    if ($coun == 0) {
        $mas = ['Имя пользователя' => $a, 'Введен email' => $b, 'Введен пароль' => $c];
        echo '<br>Регистрация прошла успешно<br>';
        foreach ($mas as $index => $item) {
            echo $index . ': ' . $item . '<br>';
        }
    }

}

?>
<form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">

        <input type="text" name="name"><br>

        <input type="email" name="email"><br>

        <input type="password" name="password"><br>

        <input type="submit" name="submit" value="Отправить">
    </form><?php
if (@$_REQUEST['submit3']) {
    $n = htmlspecialchars($_REQUEST['var']);
    if (is_numeric($n)) {
        getTextN($n);
    } else {
        echo 'Bad n';
    }
}
?>
    <form action="<?= $_SERVER['SCRIPT_NAME'] ?>">
        Введите число:<br>
        <input type="text" name="var"><br>
        <input type="submit" name="submit3" value="submit">
    </form>
<?php
function getTextN(int $n)
{
    for ($i = 0; $i < $n; $i++) {
        echo 'Silence is golden' . '<br>';
    }
}

function getArrayLenght(int $n): array
{
    $k = 0;
    $mas = [0, 1];
    $arr = [];
    while ($k != $n) {
        for ($i = 0; $i < 2; $i++) {
            $arr[] = $mas[$i];
        }
        $k++;
    }

    return $arr;
}

print_r(getArrayLenght(8));

function getRepeatElement(array $arr): array
{
    $k = 0;
    $mas = [];
    while ($k != count($arr)) {
        $t = 0;
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$k] == $arr[$i]) {
                $t++;
            }

        }
        if ($t > 1 && !in_array($arr[$k], $mas)) {
            $mas[] = $arr[$k];
        }
        $k++;
    }
    return $mas;
}

print_r(getRepeatElement([1, 2, 3, 1, 2, 5, 8, 7, 5, 2, 4, 1, 2]));

function getMinElement(float $a, float $b, float $c): float
{
    $min = $a;
    if ($min > $b) {
        $min = $b;
    }
    if ($min > $c) {
        $min = $c;
    }
    return $min;
}

function getMaxElement(float $a, float $b, float $c): float
{
    $min = $a;
    if ($min < $b) {
        $min = $b;
    }
    if ($min < $c) {
        $min = $c;
    }
    return $min;
}

echo getMinElement(3, 2, 1);

echo getMaxElement(1, 2, 3);


function getSTrapezoid(float $a, float $b, float $h): float
{
    return 0.5 * ($a + $b) * $h;
}

echo getSTrapezoid(10, 15, 6);

function getPif(float $a, float $b): float
{
    $c = $a ** 2 + $b ** 2;
    return $c ** 0.5;
}

echo getPif(6, 8);
echo '<br>13. Найти периметр' . '<br>';

function getPtriangle(float $a, float $b, float $c): float
{
    return $a + $b + $c;
}

echo getPtriangle(5, 5, 5);

function getDiscriminant(float $a, float $b, float $c)
{
    $discr = $b ** 2 - 4 * $a * $c;
    echo 'Дискриминант D = '.$discr.'<br>';
    if ($discr > 0) {
        $x1 = (-$b + ($discr * 0.5)) / (2 * $a);
        $x2 = (-$b - ($discr * 0.5)) / (2 * $a);
        echo 'x1 = ' . $x1 . ' ' . 'x2 = ' . $x2;
    } elseif ($discr == 0) {
        $x = -$b / (2 * $a);
        echo 'x = ' . $x;
    } else {
        echo 'Корней нет';
    }
}
getDiscriminant(2,4,2);

function getEvenNumber(int $n): array
{
    $mas=[];
    for($i=0;$i<$n;$i++){
        if($i%2==0){
            $mas[]=$i;
        }
    }
    return $mas;
}
print_r(getEvenNumber(100)) ;
echo '<br>15. Создать не четные числа до 100 '. '<br>';

function getNumN(int $n): array
{
    $mas=[];
    for($i=0;$i<$n;$i++){
        if($i%2!=0){
            $mas[]=$i;
        }
    }
    return $mas;
}
print_r(getNumN(100)) ;