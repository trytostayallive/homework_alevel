<?php
function getPow($var,$pow){
    $res=$var;
    for ($i=2;$i<=$pow;$i++){
        $res=$res*$var;
    }
    return $res;
}


function getSort(array $arr, string $sort='desc') :array
{
    if ($sort=='asc') {
        for ($i = 0; $i < count($arr); $i++) {
            $var = 0;
            for ($j = 0; $j < count($arr); $j++) {
                if ($arr[$j] > $arr[$i]) {
                    $var = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $var;
                }
            }
        }
    }
    if ($sort=='asc'){
        for ($i = 0; $i < count($arr); $i++) {
            $var = 0;
            for ($j = 0; $j < count($arr); $j++) {
                if ($arr[$j] < $arr[$i]) {
                    $var = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $var;
                }
            }
        }
    }
    return $arr;
}

function getSortArr(array $arr,$search)
{$res=false;
    for ($i = 0; $i < count($arr); $i++)
    {
        if ($arr[$i]==$search){
            $res=true;
        }
    }
    return $res;
}

echo getPow(3,10);

$arr=[];
for ($i=0;$i<10;$i++){
    $arr[$i]=rand(1,100);
}
var_dump(getSortArr($arr,1));

echo "<pre>";
print_r(getSort($arr,'desc'));
echo "</pre>";