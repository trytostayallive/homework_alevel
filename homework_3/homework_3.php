<?php
//@author Alexander Zh <gitlab.com/trytostayallive>
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homework3</title>
</head>
<body>

<h2>(*)</h2>

<ol>
    <li><?php echo (42 > 55 ? 42 : 55);?></li>
    <li>
        <?php
        $var = rand(1,100);
        $b = rand(1,100);
        echo "a = $var,' ', b= $b";
        echo '<br>';
        echo $var > $b ? $var : $b;
        ?>
    </li>
    <li>
        <?php
        $firstname = 'Aelx';
        $middlename = 'Zhernovnikov';
        $lastname = 'Alexandrovich';
        echo "$middlename $firstname[0].$lastname[0]";
        ?>
    </li>
    <li>
        <?php
        $randnumber= rand(1,9999);
        $searchnumber= 3;
        echo 'Количество '. $searchnumber,' в числе '.$randnumber,'= ', substr_count($randnumber,$searchnumber);
        ?>
    </li>
    <li>
        <ol>
            <?php
            $var=3;
            echo $var;
            ?>
        </ol>
        <ol>
            <?php
            $var=10;
            $b=2;
            echo 'addition= '.($var+$b), ' subtraction= '.($var-$b),' multiply= '.($var*$b),' divide= '.($var/$b);
            ?>
        </ol>
        <ol>
            <?php
            $c=15;
            $d=2;
            $result = $c+$d;
            echo $result;
            ?>
        </ol>
        <ol>
            <?php
            $var=10;
            $b=2;
            $c=5;
            echo $var+$b+$c;
            ?>
        </ol>
        <ol>
            <?php
            $var=17;
            $b=10;
            $d = 6;
            $result = $c + $d;
            echo 'result = ' . $result;

            ?>
        </ol>
    </li>
    <li>
        <?php
        $var=10;
        $b=10;
        $result=$var+$b;
        echo $result;
        ?>
    </li>
    <li>
        <?php
        $text='Hello World!';
        echo $text;
        ?>
    </li>
    <li>
        <?php
        $text1='Hello';
        $text2=' World!';
        echo $text1.$text2;
        ?>
    </li>
    <li>
        <?php
        $var=60*60;
        echo 'second in hour '.$var,'<br>';
        echo 'seconds in the day '.$var*24,'<br>';
        echo 'seconds in the week '.$var*7,'<br>';
        echo 'seconds in the month '.$var*30;
        ?>
    </li>
    <li>
        <?php
        $var = 1;
        $var += 12;
        $var -= 14;
        $var *= 5;
        $var /= 7;
        $var %= 1;
        echo $var;
        ?>
    </li>
</ol>

<h2>(**)</h2>

<ol>
    <li>
        <?php
        $sec ='12';
        $min ='52';
        $hour ='3';
        echo $hour . ':' . $min . ':' . $sec;
        ?>
    </li>
    <li>
        <?php
        $text .='Я';
        $text .=' хочу';
        $text .=' знать';
        $text .=' PHP!';
        echo $text;
        ?>
    </li>
    <li>
        <?php
        $foo = 'bar';
        $bar = 10;
        echo $$foo;
        ?>
    </li>
    <li>
        Какой будет результат если a = 2; $b = 4;echo a++ + $b;echo a + ++$b;a ++a + $b++;
        <br>
        6,8,9
    </li>
    <li>
        <ul>
            <li>isset($var)</li>
            <?php
            $var = 123;
            echo isset($var) ? 'exist' : 'not exist';
            ?>
            <li>gettype($var)</li>
            <?php
            $var = '123';
            echo gettype($var) == 'string' ? 'thats a string' : 'thats not string';
            ?>
            <li>is_null($var)</li>
            <?php
            $var = 3;
            echo is_null($var) ? 'NULL' : 'not NULL';
            ?>
            <li>empty($var)</li>
            <?php
            $var = '';
            echo empty($var) ? 'empty' : 'not empty';
            ?>
            <li>is_integer($var)</li>
            <?php
            $var = 3.5;
            echo is_integer($var) ? 'Thats integer' : 'Thats not integer';
            ?>
            <li>is_double($var)</li>
            <?php
            $var = 3.5;
            echo is_double($var) ? 'Thats double' : 'Thats not double';
            ?>
            <li>is_numeric($var)</li>
            <?php
            $var = '5а';
            echo is_numeric($var) ? 'Thats numeric' : 'Thats not numeric';
            ?>
            <li>is_bool($var)</li>
            <?php
            $var = true;
            echo is_bool($var) ? 'Thats boolean' : 'Thats not boolean';
            ?>
            <li>is_scalar($var)</li>
            <?php
            $var = null;
            echo is_scalar($var) ? 'Thats scalar' : 'Thats not scalar';
            ?>
            <li>is_array($var)</li>
            <?php
            $var = [1, 2, 3];
            echo is_array($var) ? 'Thats array' : 'Thats not array';
            ?>
            <li>is_object($var)</li>
            <?php
            $var = (object)$var;
            echo is_object($var) ? 'Thats object' : 'Thats not object';
            ?>
        </ul>
    </li>
</ol>

<h2>(***)</h2>
<ol>
    <li>
        <?php
        // Create three variable for use in the next tasks (1-3)
        $a=rand(1,10);
        $b=rand(1,10);
        $c=rand(1,10);
        echo 'a= '.$a, 'b= '.$b,'c= '.$c , '<br> adition a+b= '.($a+$b), '<br> multiply a+b= '.($a*$b);
        ?>
    </li>
    <li>
        <?php
        echo pow($a,2)+pow($b,2);
        ?>
    </li>
    <li>
        <?php
        echo ($a+$b+$c)/3;
        ?>
    </li>
    <li>
        <?php
        $x=1;
        $y=2;
        $z=3;
        echo ($x+1)-2*($z - 2 * $x +$y);
        ?>
    </li>
    <li>
        <?php
            $var= 10;
            echo $var.' div 3 = '.($var%3),' <br> div 5 = '.($var%5);
            echo  '<br>+30% ='.($var+$var*0.3);
            echo  '<br>+120% ='.($var+$var*1.2);
        ?>
    </li>
    <li>
        <?php
        $var1=100;
        $var2=200;
        echo 'addition 40% of '.$var1.' and 84% of '.$var2.' = '.(($var1*0.40)+($var2*0.84));
        $var1=(string)$var1;
        echo '<br>'.($var1[0]+$var1[1]+$var[2]);
        ?>
    </li>
    <li>
        <?php
        $var =111;
        $var = (string)$var;
        $var[1]='0';
        echo $var[2].$var[1].$var[0];
        ?>
    </li>
    <li>
        <?php
        $var = 8;
        $secondvar = 7;
        echo $var % 2 == 0 ? 'четное' : 'не четное';
        echo '<br>';
        echo $secondvar % 2 == 0 ? 'четное' : 'не четное';
        ?>
    </li>
</ol>
</body>
</html>
