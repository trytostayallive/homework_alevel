<?php
(string)$name= 'alex';
$age= 20;
$pi= pi();
$array_1=['alex', 'vova', 'tolya'];
$array_2=['alex', 'vova', 'tolya',
         ['kostya', 'olya']
         ];
$array_3=['alex', 'vova', 'tolya',
         ['kostya', 'olya',
         ['gosha', 'mila']]];
$array_4=[['alex', 'vova', 'tolya'],
         ['kostya', 'olya'],
         ['gosha', 'mila']];

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homework</title>
</head>
<body>
<ol>
    <li><?= $name ?></li>
    <li><?= $age ?></li>
    <li><?= $pi ?></li>
    <li>
        <pre><?php print_r($array_1)?></pre>
    </li>
    <li>
        <pre><?php print_r($array_2)?></pre>
    </li>
    <li>
        <pre><?php print_r($array_3)?></pre>
    </li>
    <li>
        <pre><?php print_r($array_4)?></pre>
    </li>
</ol>
</body>
</html>
